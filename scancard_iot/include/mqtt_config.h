#ifndef __mqtt_config_
#define __mqtt_config_


//MQTT 设备: 基于OpenHarmony的刷卡考勤系统
#define CLIENT_ID "60731462594ebc02c03c34ee_2021-4-11_0_0_2021041115"
#define USERNAME "60731462594ebc02c03c34ee_2021-4-11"
#define PASSWORD "8aa6e540c7f5cb01b6c9ac6fd0818b2fd7de1d3727b892003b58b42a981d1d78"


typedef struct
{ // object data type
    char *Buf;
    uint8_t Idx;
} MSGQUEUE_OBJ_t;

typedef enum
{
    en_msg_cmd = 0,
    en_msg_report,
} en_msg_type_t;

typedef struct
{
    char *request_id;
    char *payload;
} cmd_t;

typedef struct
{
    // int lum;
    // int temp;
    // int hum;
    unsigned char *scanRecord[100];
} report_t;

typedef struct
{
    en_msg_type_t msg_type;
    union
    {
        cmd_t cmd;
        report_t report;
    } msg;
} app_msg_t;


void deal_report_msg(report_t *report);
void oc_cmd_rsp_cb(uint8_t *recv_data, size_t recv_size, uint8_t **resp_data, size_t *resp_size);


#endif