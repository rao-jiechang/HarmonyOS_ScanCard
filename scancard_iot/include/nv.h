#ifndef __nv_h__
#define __nv_h__


// hi_u32 hi_u8 要用到
#include <hi_types_base.h>
#include "list.h"

/* 作废 */
#define NV_ID 0x0B
#define NV_ID_2 0x0C    // 用户信息（除name之外的所有信息）
#define NV_ID_3 0x0D    // 用户信息（只包含name的信息）
#define NV_ID_4 0x0E    // 用户数量

/* 现用 */
#define NV_ID_USER_NAME 0xB0

#define NV_ID_USERINFO_1 0xB1

#define NV_ID_USERINFO_2 0xB2
#define NV_ID_USERINFO_3 0xB3

#define NV_ID_USERINFO_4 0xB4
#define NV_ID_USERINFO_5 0xB5
#define NV_ID_USERINFO_6 0xB6
#define NV_ID_USERINFO_7 0xB7

#define NV_ID_USERINFO_8 0xB8
#define NV_ID_USERINFO_9 0xB9
#define NV_ID_USERINFO_10 0xBA
#define NV_ID_USERINFO_11 0xBB
#define NV_ID_USERINFO_12 0xBC
#define NV_ID_USERINFO_13 0xBD
#define NV_ID_USERINFO_14 0xBE

// #define NV_ID_USER_NAME 0x0C

// #define NV_ID_USERINFO_1 0x0E

// #define NV_ID_USERINFO_2 0x0F
// #define NV_ID_USERINFO_3 0x10

// #define NV_ID_USERINFO_4 0x11
// #define NV_ID_USERINFO_5 0x12
// #define NV_ID_USERINFO_6 0x13
// #define NV_ID_USERINFO_7 0x14

// #define NV_ID_USERINFO_8 0x15
// #define NV_ID_USERINFO_9 0x16
// #define NV_ID_USERINFO_10 0x17
// #define NV_ID_USERINFO_11 0x18
// #define NV_ID_USERINFO_12 0x19
// #define NV_ID_USERINFO_13 0x1A
// #define NV_ID_USERINFO_14 0x1B


//-- test --//
#define TEST_SIZE 32
#define TEST_FLASH_OFFSET 0x100000



/* Database store into Flash */
#define FLASH_USERINFO_OFFSET 0x100000
#define FLASH_USERINFO_SIZE 40

/* 结构 */
//---------------------------------------
//分配给每个用户40Bytes
//userid       --->  4 Bytes
//其他(13个项)  ---> 13 Bytes
//剩余全是name  ---> 23 Bytes
//---------------------------------------
//用户1 起始地址 [FLASH_USERINFO_OFFSET]
//userid : FLASH_USERINFO_OFFSET      0x100000
//others : FLASH_USERINFO_OFFSET+4    0x100004
//name   : FLASH_USERINFO_OFFSET+4+13 0x100011
//---------------------------------------
//用户i 起始地址 [FLASH_USERINFO_OFFSET + 40*i]
#define FLASH_USERINFO_USER1 0x100000           //用户1 起始地址
#define FLASH_USERINFO_USER2 (0x100000 + 40)    //用户2 起始地址
#define FLASH_USERINFO_USER3 (0x100000 + 80)    //用户3 起始地址
#define FLASH_USERINFO_USER4 (0x100000 + 120)   //用户4 起始地址
#define FLASH_USERINFO_USER5 (0x100000 + 160)   //用户5 起始地址
#define FLASH_USERINFO_USER_NUM 0x102000        //用户数量 起始地址
#define FLASH_SCANCARD_TIME 0x104000            //打卡时间 起始地址
#define FLASH_WIFI_SSID 0x106000                //Wi-Fi ssid 起始地址
#define FLASH_WIFI_PASSWD 0x108000              //Wi-Fi passwd 起始地址







typedef struct {
    hi_u8 ssid[50];
    hi_u8 passwd[50];
} wal_cfg_ssid_my;

void NV_Test(void);
void Flash_Test(void);

void Flash_Write(const hi_u32 flash_offset, LinkList L, hi_u8 user_num, hi_bool do_erase);
void Flash_Read(const hi_u32 flash_offset, LinkList L, hi_u8 *user_num);

void Flash_Write_Scancard(const hi_u32 flash_offset, hi_bool do_erase);
void Flash_Read_Scancard(const hi_u32 flash_offset);


#endif