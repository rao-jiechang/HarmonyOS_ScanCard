#ifndef __uart_h__
#define __uart_h__

#define UART_PORT WIFI_IOT_UART_IDX_1
#define UART_BUFF_SIZE 250


void Uart_GPIO_Init(void);
void Uart_Config(void);
void Uart_Init(void);
void Uart_Test(void);

hi_s32 Uart_Read_Data(hi_u8 *data, hi_u32 *data_len);                               //串口读取
hi_s32 Uart_Read_Data_Timeout(hi_u8 *data, hi_u32 *data_len, hi_u32 timeout_ms);    //超时串口读取
void Uart_Write_Data(hi_u8 *data, hi_u32 data_len);                                 //串口发送



#endif