#ifndef __spi_h__
#define __spi_h__


#define SPI_PORT WIFI_IOT_SPI_ID_0


void SPI0_Init(void);
unsigned char SPI0_ReadWriteByte(unsigned char TxData);


#endif