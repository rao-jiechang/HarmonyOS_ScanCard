#ifndef __display_screen_h__
#define __display_screen_h__


#define DISPLAY_SCREEN_SCANRECORD
#define DISPLAY_SCREEN_ADDUSER
#define DISPLAY_SCREEN_DELUSER
// #define DISPLAY_SCREEN_LOGIN
#define DISPLAY_SCREEN_PERMISSION


//回调函数 函数指针
typedef void (*display_screen_cmd_call_back_func)(void);

//串口屏命令结构
typedef struct {
    hi_char *display_screen_cmd_name;
    hi_s8   display_screen_cmd_len;
    display_screen_cmd_call_back_func display_screen_cmd_func;
} display_screen_cmd;




hi_s32 Display_Screen_Read(hi_u8 *data, hi_u32 *data_len);
hi_s32 Display_Screen_Read_Timeout(hi_u8 *data, hi_u32 *data_len, hi_u32 timeout_ms);
void Display_Screen_Write(hi_u8 *data);
void Display_Screen_cmd_callback(display_screen_cmd_call_back_func func);



// void Display_Screen_Write_GB2312(hi_u8 mode);
void Display_Screen_Write_Custom(hi_u8 *data);
void Display_Screen_Write_Time(hi_u16 time, hi_u8 widget_index);
void Display_Screen_Write_Time_clock_in_time(hi_u16 time, hi_u8 widget_index);
// void Display_Screen_Write_Name_tmp(void);
// void Display_Screen_Write_utf8_to_gb2312(hi_u8 *data_in_utf8, hi_u8 *name);



#endif