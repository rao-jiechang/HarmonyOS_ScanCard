#ifndef __list_h__
#define __list_h__


#define NAME_BYTE_LEN 20
// #define NAME_MAX (NAME_BYTE_LEN/2-1)


/**
 * @brief 时长状态
*/
typedef enum {
    NO_NO = 0,                      /**< 否 */
    YES_YES,                        /**< 是 */
} TimeIsForever;


typedef unsigned int ElemType;          //元素类型：整型 4Byte
typedef struct UserInfo {
    /* ID */
    ElemType userid;                            //卡号ID
    // unsigned char name[8];                      //中文用户名，最多4个汉字，gb2312 编码，一个汉字占用2个字节(16进制)，多余的用0x00代替，需要发送时直接把8个字节的name发出
    // unsigned char name[12];                      //中文用户名，最多4个汉字，utf-8 编码，一个汉字占用3个字节(16进制)，多余的用0x00代替，需要发送时直接把12个字节的name发出
    // unsigned char name[NAME_BYTE_LEN];          //中文用户名，最多4个汉字，gb2312 编码，一个汉字占用2个字节(16进制)，多余的用0x00代替，需要发送时直接把8个字节的name发出
    //                                             // 这里5个汉字，多出一个，是为了避免出现bug，打印出多余的Byte
    unsigned char name[NAME_BYTE_LEN];

    /* 权限 */
    unsigned char permission;                   //权限
    unsigned char permission_last;              //上一次权限，用于临时存储权限信息
    /* 权限时长 */
    unsigned char permission_time_is_forever;   //是否永远生效 {YES_YES}永远生效； {NO_NO}不是永远，可设置期限 time_hours time_minutes time_seconds
    unsigned char permission_time_hours;        //时 0-24
    unsigned char permission_time_minutes;      //分 0-60
    unsigned char permission_time_seconds;      //秒 0-60

    /* 刷卡/打卡时间 */
    unsigned char scancard_time_is_same;        //是否与系统打卡时间一致 {YES_YES}一致； {NO_NO}自定义，可设置期限 time_hours time_minutes time_seconds
    unsigned char morning_time_hours;           //时 0-24
    unsigned char morning_time_minutes;         //分 0-60
    unsigned char morning_time_seconds;         //秒 0-60
    unsigned char afternoon_time_hours;         //时 0-24
    unsigned char afternoon_time_minutes;       //分 0-60
    unsigned char afternoon_time_seconds;       //秒 0-60
} UserInfo;

typedef struct Node {

    struct UserInfo data; //用户信息
    // ElemType index; //0代表头结点，从1开始
    struct Node *next;
} Node, *LinkList;

// 系统时间
struct sys_time
{
    /* 年月日 */
    unsigned int  sys_time_years;   //2021
    unsigned char sys_time_months;  //1-12
    unsigned char sys_time_days;    //1-31
    /* 时分秒 */
    unsigned char sys_time_hours;   //0-23
    unsigned char sys_time_minutes; //0-59
    unsigned char sys_time_seconds; //0-59
};

// 打卡时间
struct scancard_time
{
    /* 时分秒 早上 */
    unsigned char  morning_time_hours;    //0-23
    unsigned char morning_time_minutes;  //0-59
    unsigned char morning_time_seconds;  //0-59
    /* 时分秒 下午 */
    unsigned char afternoon_time_hours;   //0-23
    unsigned char afternoon_time_minutes; //0-59
    unsigned char afternoon_time_seconds; //0-59
};

// 考勤记录(信息)
struct sys_record_info
{
    //time  /* 年月日 */ /* 时分秒 */
    unsigned int  record_time_years;    //2021
    unsigned char record_time_months;   //1-12
    unsigned char record_time_days;     //1-31
    unsigned char record_time_hours;    //0-23
    unsigned char record_time_minutes;  //0-59
    unsigned char record_time_seconds;  //0-59
    //name
    unsigned char name[NAME_BYTE_LEN];
    //权限
    unsigned char authority;
    //id
    unsigned int  id;
};




LinkList CreateListHead(void);                      //创建一个链表
int Insert(UserInfo *info, LinkList L);             //插入结点
int Delete(UserInfo *info, LinkList L);             //删除结点
int Find_Permission(UserInfo *info, LinkList L);    //查找结点并返回权限
LinkList Find(UserInfo *info, LinkList L);          //查找结点
void PrintfList(LinkList L);                        //打印链表
char Get_List_Num(LinkList L);                      //得到链表的结点数量

void LinkList_Test(void);                           //测试
void Permission_Time_Pros(LinkList L);                         //时间处理



#endif