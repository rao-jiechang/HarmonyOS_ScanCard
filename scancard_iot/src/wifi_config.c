#include "wifi_config.h"

#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "lwip/netif.h"
#include "lwip/netifapi.h"
#include "lwip/ip4_addr.h"
#include "lwip/api_shell.h"

#include "cmsis_os2.h"
#include "hos_types.h"
#include "wifi_device.h"
#include "wifiiot_errno.h"
#include "ohos_init.h"


#define SELECT_WLAN_PORT "wlan0"
#define SELECT_WIFI_SSID "nova 5z"
#define SELECT_WIFI_PASSWORD "00000000"
#define SELECT_WIFI_SECURITYTYPE WIFI_SEC_TYPE_PSK


#define DEF_TIMEOUT 15
#define ONE_SECOND 1


static int g_staScanSuccess = 0; //Wifi 扫描状态，1为成功
static int g_ConnectSuccess = 0; //Wifi 连接状态，1为成功
static int ssid_count = 0; //Wifi ssid 个数
WifiEvent g_wifiEventHandler = {0};
WifiErrorCode error;


// ----- Wi-Fi Callback Function start ----- //
static void OnWifiScanStateChangedHandler(int state, int size)
{
    (void)state;

    printf("Wi-Fi Callback Function -> OnWifiScanStateChangedHandler\r\n");

    if (size > 0)
    {
        ssid_count = size;
        g_staScanSuccess = 1; //标志:Wifi 扫描成功
    }
    return;
}

static void OnWifiConnectionChangedHandler(int state, WifiLinkedInfo *info)
{
    (void)info;

    printf("Wi-Fi Callback Function -> OnWifiConnectionChangedHandler\r\n");

    if (state > 0)
    {
        g_ConnectSuccess = 1; //标志:Wifi 连接成功
        printf("callback function for wifi connect\r\n");
    }
    else
    {
        printf("connect error,please check password\r\n");
    }
    return;
}

static void OnHotspotStaJoinHandler(StationInfo *info)
{
    (void)info;

    printf("Wi-Fi Callback Function -> OnHotspotStaJoinHandler\r\n");

    printf("STA join AP\r\n");
    return;
}

static void OnHotspotStaLeaveHandler(StationInfo *info)
{
    (void)info;

    printf("Wi-Fi Callback Function -> OnHotspotStaLeaveHandler\r\n");

    printf("HotspotStaLeave:info is null.\r\n");
    return;
}

static void OnHotspotStateChangedHandler(int state)
{
    printf("Wi-Fi Callback Function -> OnHotspotStateChangedHandler\r\n");

    printf("HotspotStateChanged:state is %d.\r\n", state);
    return;
}
// ----- Wi-Fi Callback Function start ----- //

//Wi-Fi 初始化
void WiFiInit(void)
{
    printf("<--Wifi Init-->\r\n");
    g_wifiEventHandler.OnWifiScanStateChanged = OnWifiScanStateChangedHandler;
    g_wifiEventHandler.OnWifiConnectionChanged = OnWifiConnectionChangedHandler;
    g_wifiEventHandler.OnHotspotStaJoin = OnHotspotStaJoinHandler;
    g_wifiEventHandler.OnHotspotStaLeave = OnHotspotStaLeaveHandler;
    g_wifiEventHandler.OnHotspotStateChanged = OnHotspotStateChangedHandler;
    error = RegisterWifiEvent(&g_wifiEventHandler);
    if (error != WIFI_SUCCESS)
    {
        printf("register wifi event fail!\r\n");
    }
    else
    {
        printf("register wifi event succeed!\r\n");
    }
}

static void WaitSacnResult(void)
{
    int scanTimeout = DEF_TIMEOUT; //最长等15s
    while (scanTimeout > 0)
    {
        sleep(ONE_SECOND);
        scanTimeout--;
        if (g_staScanSuccess == 1)
        {
            printf("WaitSacnResult:wait success[%d]s.\r\n", (DEF_TIMEOUT - scanTimeout));
            break;
        }
    }
    if (scanTimeout <= 0)
    {
        printf("WaitSacnResult:timeout!\r\n");
    }
}

static int WaitConnectResult(void)
{
    int ConnectTimeout = DEF_TIMEOUT;
    while (ConnectTimeout > 0)
    {
        sleep(1);
        ConnectTimeout--;
        if (g_ConnectSuccess == 1)
        {
            printf("WaitConnectResult:wait success[%d]s.\r\n", (DEF_TIMEOUT - ConnectTimeout));
            break;
        }
    }
    if (ConnectTimeout <= 0)
    {
        printf("WaitConnectResult:timeout!\r\n");
        return 0;
    }

    return 1;
}


// char Wifi_Connect(unsigned char *ssid, unsigned char *pwd)
char Wifi_Connect_Test(void)
{
    WifiScanInfo *info = NULL;
    unsigned int size = WIFI_SCAN_HOTSPOT_LIMIT;
    WifiDeviceConfig select_ap_config = {0};
    static struct netif *g_lwip_netif = NULL;

    unsigned char i;
    int result;

    usleep(200000);

    //初始化WIFI
    WiFiInit();

    //使能WIFI
    if (EnableWifi() != WIFI_SUCCESS)
    {
        printf("EnableWifi failed, error = %d\r\n", error);
        return -1;
    }

    //判断WIFI是否激活
    if (IsWifiActive() == 0)
    {
        printf("Wifi station is not actived.\r\n");
        return -1;
    }

    //分配空间，保存WiFi信息
    info = malloc(sizeof(WifiScanInfo) * WIFI_SCAN_HOTSPOT_LIMIT);
    if (info == NULL)
    {
        printf("info == NULL\r\n");
        return -1;
    }

    //轮询查找WiFi列表
    do{
        //重置标志位
        ssid_count = 0;
        g_staScanSuccess = 0;

        //开始扫描
        Scan();

        //等待扫描结果
        WaitSacnResult();

        //获取扫描列表
        error = GetScanInfoList(info, &size);

    }while(g_staScanSuccess != 1);

    //打印WiFi列表
    printf("********************\r\n");
    for(i = 0; i < ssid_count; i++)
    {
        // %03d  左对齐，左补0
        // %-30s 左对齐，输出30个字符串
        // %5d   右对齐，左补空格
        printf("no:%03d, ssid:%-30s, rssi:%5d\r\n", i+1, info[i].ssid, info[i].rssi/100);
    }
    printf("********************\r\n");

    //连接指定的WiFi热点
    for(i = 0; i < ssid_count; i++)
    {
        if (strcmp(SELECT_WIFI_SSID, info[i].ssid) == 0)
        {
            printf("Select:%3d wireless, Waiting...\r\n", i+1);

            //拷贝要连接的热点信息
            strcpy(select_ap_config.ssid, info[i].ssid);
            strcpy(select_ap_config.preSharedKey, SELECT_WIFI_PASSWORD);
            select_ap_config.securityType = SELECT_WIFI_SECURITYTYPE;

            if (AddDeviceConfig(&select_ap_config, &result) == WIFI_SUCCESS)
            {
                if (ConnectTo(result) == WIFI_SUCCESS && WaitConnectResult() == 1)
                {
                    printf("WiFi connect succeed!\r\n");
                    g_lwip_netif = netifapi_netif_find(SELECT_WLAN_PORT);
                    break;
                }
            }
        }

        if(i == ssid_count-1)
        {
            printf("ERROR: No wifi as expected\r\n");
            while(1) osDelay(100);
        }
    }

    //启动DHCP
    if (g_lwip_netif)
    {
        dhcp_start(g_lwip_netif);
        printf("begain to dhcp");
    }

    //等待DHCP
    for(;;)
    {
        if(dhcp_is_bound(g_lwip_netif) == ERR_OK)
        {
            printf("<-- DHCP state:OK -->\r\n");

            //打印获取到的IP信息
            netifapi_netif_common(g_lwip_netif, dhcp_clients_info_show, NULL);
            break;
        }

        printf("<-- DHCP state:Inprogress -->\r\n");
        osDelay(100);
    }

    //执行其他操作
    for(;;)
    {
        osDelay(100);
    }
}


/**
 * @brief  Wifi 连接
 * @par  Wifi 连接函数，并通过DHCP分配IP
 * @param  ssid  [IN] type #const char* wifi 账号:ssid
 * @param  pwd   [OUT] type #const char* wifi 密码:passwd
 * @retval  #0   Wifi 连接成功
 * @retval  #-1  Wifi 使能失败
 * @retval  #-2  Wifi 激活失败
 * @retval  #-3  malloc 分配空间失败(用于保存WiFi信息)
 * @retval  #-4  Wifi 连接失败，扫描直到最后一个Wifi，没有匹配的
*/
int Wifi_Connect(const char *ssid, const char *pwd)
{
	WifiScanInfo *info = NULL;
    unsigned int size = WIFI_SCAN_HOTSPOT_LIMIT;
    static struct netif *g_lwip_netif = NULL;
    WifiDeviceConfig select_ap_config = {0};

    unsigned char i;
    int result;

    osDelay(200);
    printf("<----- System Init ----->\r\n");

    //初始化WIFI
    WiFiInit();

    //使能WIFI
    if (EnableWifi() != WIFI_SUCCESS)
    {
        printf("EnableWifi failed, error = %d\r\n", error);
        return -1;
    }

    //判断WIFI是否激活
    if (IsWifiActive() == 0)
    {
        printf("Wifi station is not actived.\r\n");
        return -2;
    }

    //分配空间，用于保存WiFi信息
    info = malloc(sizeof(WifiScanInfo) * WIFI_SCAN_HOTSPOT_LIMIT);
    if (info == NULL)
    {
        return -3;
    }

    //轮询查找WiFi列表
    do{
        //重置标志位
        ssid_count = 0;
        g_staScanSuccess = 0;

        //开始扫描
        Scan();

        //等待扫描结果
        WaitSacnResult();

        //获取扫描列表
        error = GetScanInfoList(info, &size);

    }while(g_staScanSuccess != 1);

    //打印WiFi列表
    printf("********************\r\n");
    for(i = 0; i < ssid_count; i++)
    {
        printf("no:%03d, ssid:%-30s, rssi:%5d\r\n", i+1, info[i].ssid, info[i].rssi/100);
    }
    printf("********************\r\n");

    //连接指定的WiFi热点
    for(i = 0; i < ssid_count; i++)
    {
        if (strcmp(ssid, info[i].ssid) == 0)
        {
            printf("Select:%3d wireless, Waiting...\r\n", i+1);

            //拷贝要连接的热点信息
            strcpy(select_ap_config.ssid, info[i].ssid);
            strcpy(select_ap_config.preSharedKey, pwd);
            select_ap_config.securityType = SELECT_WIFI_SECURITYTYPE;

            if (AddDeviceConfig(&select_ap_config, &result) == WIFI_SUCCESS)
            {
                if (ConnectTo(result) == WIFI_SUCCESS && WaitConnectResult() == 1)
                {
                    printf("WiFi connect succeed!\r\n");
                    g_lwip_netif = netifapi_netif_find(SELECT_WLAN_PORT);
                    break;
                }
            }
        }
        // 扫描到最后一个Wifi，没有匹配的
        if(i == ssid_count-1)
        {
            printf("ERROR: No wifi as expected\r\n");
            
            return -4;
        }
    }

    //启动DHCP
    if (g_lwip_netif)
    {
        dhcp_start(g_lwip_netif);
        printf("begain to dhcp\r\n");
    }

    //等待DHCP
    for(;;)
    {
        if(dhcp_is_bound(g_lwip_netif) == ERR_OK)
        {
            // DHCP state:OK
            printf("<-- DHCP state:OK -->\r\n");

            //打印获取到的IP信息
            netifapi_netif_common(g_lwip_netif, dhcp_clients_info_show, NULL);
            break;
        }

        // DHCP state:正在进行中
        printf("<-- DHCP state:Inprogress -->\r\n");
        osDelay(100);
    }

    osDelay(100);

	return 0; //Wifi 连接成功
}

/**
 * @brief  Wifi 断开连接
 * @par  断开 Wifi 连接
*/
void Wifi_Disconnect(void)
{
    Disconnect();
}