#include <stdio.h>
#include <unistd.h>

#include <hi_types_base.h>
//串口
#include "wifiiot_uart.h"
#include "wifiiot_uart_ex.h"
#include "hi_uart.h"
//GPIO
#include "wifiiot_gpio.h"
#include "wifiiot_errno.h"
#include "wifiiot_gpio_ex.h"

#include "uart.h"





//配置IO复用：将对应的IO复用为UART的TX、RX、RTS、CTS功能。
void Uart_GPIO_Init(void)
{
    /* 如下IO复用配置，也可集中在SDK中的app_io_init函数中进行配置 */
    IoSetFunc(WIFI_IOT_IO_NAME_GPIO_0, WIFI_IOT_IO_FUNC_GPIO_0_UART1_TXD); /* uart1 tx */
    IoSetFunc(WIFI_IOT_IO_NAME_GPIO_1, WIFI_IOT_IO_FUNC_GPIO_1_UART1_RXD); /* uart1 rx */
}

void Uart_Config(void)
{
    hi_u32 ret;
    WifiIotUartAttribute uart_attr = {
        .baudRate = 115200, /* 波特率: 115200 */
        .dataBits = 8,      /* 数据位: 8bits */
        .stopBits = 1,      /* 停止位: 1bits */
        .parity = 0,        /* 校验位: 无校验 */
        .pad = 0
    };

    ret = UartInit(UART_PORT, &uart_attr, HI_NULL);
    // if (ret != HI_ERR_SUCCESS) {
    if (ret != WIFI_IOT_SUCCESS) {
        printf("Failed to init uart %d! Err code = %d\n", UART_PORT, ret);
    }
    else
    {
        printf("Initialize uart %d successfully\n", UART_PORT);
    }
}

void Uart_Init(void)
{
    Uart_GPIO_Init();
    Uart_Config();
}

void Uart_Test(void)
{
    hi_u8 uart_buff[18] = "main.t0.txt=\"123\"";
    hi_u8 *uart_buff_ptr = uart_buff;
    hi_u8 uart_buff_tmp[4] = {0xFF,0xFF,0xFF,0xFF};

    UartWrite(UART_PORT, uart_buff_ptr, 17);
    UartWrite(UART_PORT, uart_buff_tmp, 3);
}

//阻塞 串口读数据
hi_s32 Uart_Read_Data(hi_u8 *data, hi_u32 *data_len)
{
    hi_s32 len;

    len = UartRead(UART_PORT, data, UART_BUFF_SIZE);
    if ((len < 0) || (0 == len)) {
        printf("UartRead fail >>> %s %d\r\n", __FILE__, __LINE__);
        return -1;
    }

    *data_len = len;

    return 0;
}

//超时限制 串口读数据
hi_s32 Uart_Read_Data_Timeout(hi_u8 *data, hi_u32 *data_len, hi_u32 timeout_ms)
{
    hi_s32 len;

    len = hi_uart_read_timeout(UART_PORT, data, UART_BUFF_SIZE, timeout_ms);
    if ((len < 0) || (0 == len)) {
        // printf("hi_uart_read_timeout fail >>> %s %d\r\n", __FILE__, __LINE__);
        return -1;
    }

    *data_len = len;

    return 0;
}

void Uart_Write_Data(hi_u8 *data, hi_u32 data_len)
{
    UartWrite(UART_PORT, data, data_len);
}

