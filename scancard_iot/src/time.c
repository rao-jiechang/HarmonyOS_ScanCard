#include "time.h"
#include "list.h"


extern struct sys_time Sys_Time;


void SysTime_Pros(void)
{
    Sys_Time.sys_time_seconds++;
    if(Sys_Time.sys_time_seconds >= 60)
    {
        Sys_Time.sys_time_seconds = 0;
        Sys_Time.sys_time_minutes++;
        if(Sys_Time.sys_time_minutes >= 60)
        {
            Sys_Time.sys_time_minutes = 0;
            Sys_Time.sys_time_hours++;
            if(Sys_Time.sys_time_hours >= 24)
            {
                Sys_Time.sys_time_hours = 0;
                Sys_Time.sys_time_days++;
                if(Sys_Time.sys_time_days >= 31)
                {
                    Sys_Time.sys_time_days = 0;
                }
            }
        }
    }
}


