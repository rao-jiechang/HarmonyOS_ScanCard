#include <stdio.h>
#include <unistd.h>
#include <string.h>

#include <malloc.h>
#include "cmsis_os2.h"

#include "oc_mqtt.h"
#include "mqtt_config.h"


extern osMessageQueueId_t mid_MsgQueue;


void deal_report_msg(report_t *report)
{
    oc_mqtt_profile_service_t service;
    oc_mqtt_profile_kv_t attr_scanRecord;

    service.event_time = NULL;
    service.service_id = "Office_Management";
    service.service_property = &attr_scanRecord;
    service.nxt = NULL;

    attr_scanRecord.key = "Scan_Record";
    attr_scanRecord.value = &report->scanRecord[0];
    attr_scanRecord.type = EN_OC_MQTT_PROFILE_VALUE_STRING;
    attr_scanRecord.nxt = NULL;

    oc_mqtt_profile_propertyreport(USERNAME, &service);
    return;
}

void oc_cmd_rsp_cb(uint8_t *recv_data, size_t recv_size, uint8_t **resp_data, size_t *resp_size)
{
    app_msg_t *app_msg;

    int ret = 0;
    app_msg = malloc(sizeof(app_msg_t));
    app_msg->msg_type = en_msg_cmd;
    app_msg->msg.cmd.payload = (char *)recv_data;

    printf("recv data is %.*s\n", recv_size, recv_data);
    ret = osMessageQueuePut(mid_MsgQueue, &app_msg, 0U, 0U);
    if (ret != 0)
    {
        free(recv_data);
    }
    *resp_data = NULL;
    *resp_size = 0;
}
