#include <stdio.h>
#include <unistd.h>
#include "string.h"

#include <hi_types_base.h>

#include "uart.h"
#include "display_screen.h"


// #include <stdlib.h>
// #include <stddef.h>
// #include "libiconv/iconv.h"
// #include <libiconv_include/iconv.h>
// #include "Z://code-1.0/applications/sample/wifi-iot/app/5_scancard_iot/libiconv_include/iconv.h"


//写入串口屏
void Display_Screen_Write(hi_u8 *data)
{
    hi_u8 tmp[3] = {0xFF, 0xFF, 0xFF};

    Uart_Write_Data(data, strlen((const char*)data));
    Uart_Write_Data(tmp, 3);
}

//从串口屏读取
hi_s32 Display_Screen_Read(hi_u8 *data, hi_u32 *data_len)
{
    return Uart_Read_Data(data, data_len);
}


//从串口屏超时读取
hi_s32 Display_Screen_Read_Timeout(hi_u8 *data, hi_u32 *data_len, hi_u32 timeout_ms)
{
    return Uart_Read_Data_Timeout(data, data_len, timeout_ms);
}


//回调函数，收到串口屏指令调用
void Display_Screen_cmd_callback(display_screen_cmd_call_back_func func)
{
    func();
}




////////////////////////////////////////////////////////////////////////////
//串口屏显示
//显示内容：
//"t0.txt+=\""
//"......" 具体内容
//"\""
void Display_Screen_Write_Custom(hi_u8 *data)
{
    hi_u8 tmp[3] = {0xFF, 0xFF, 0xFF};

    hi_u8 buf1[8] = {0x74, 0x30, 0x2E, 0x74, 0x78, 0x74, 0x2B, 0x3D};
    hi_u8 buf2[1] = {0x22};

    Uart_Write_Data(buf1, 8);
    Uart_Write_Data(buf2, 1);
    //name ... 可自定制
    Uart_Write_Data(data, strlen((const char *)data));

    Uart_Write_Data(buf2, 1);
    Uart_Write_Data(tmp, 3);
}

//串口屏显示
//显示内容：
//"system_time.nx.val=具体内容(数字，比如2021,3,31)
void Display_Screen_Write_Time(hi_u16 time, hi_u8 widget_index)
{
    unsigned char str[23] = {0};
    hi_u8 tmp[3] = {0xFF, 0xFF, 0xFF};

    //time ... 可自定制
    sprintf((char *)str, "system_time.n%d.val=%d", widget_index, time); //system_time:11
    Uart_Write_Data(str, strlen((const char *)str));

    Uart_Write_Data(tmp, 3);
}

//串口屏显示
//显示内容：
//"clock_in_time.nx.val=具体内容(数字，比如8,29,59)
void Display_Screen_Write_Time_clock_in_time(hi_u16 time, hi_u8 widget_index)
{
    unsigned char str[23] = {0};
    hi_u8 tmp[3] = {0xFF, 0xFF, 0xFF};

    //time ... 可自定制
    sprintf((char *)str, "clock_in_time.n%d.val=%d", widget_index, time); //clock_in_time:13
    Uart_Write_Data(str, strlen((const char *)str));

    Uart_Write_Data(tmp, 3);
}

////////////////////////////////////////////////////////////////////////////

//写入串口屏--name
//utf-8
//name：用户数据的name指针
// void Display_Screen_Write_Name(hi_u8 *name)
// {
//     hi_u8 i;
//     // Display_Screen_Write((hi_u8 *)"t0.txt+="饶皆昌");
//     hi_u8 buf[34] = {0x74, 0x30, 0x2E, 0x74, 0x78, 0x74, 0x2B, 0x3D, 0x22, 
//     //姓名：E5A793 E5908D EFBC9A
//     0xE5, 0xA7, 0x93, 
//     0xE5, 0x90, 0x8D, 
//     0xEF, 0xBC, 0x9A, 
//     //name[0]~name[12]
//     0xE9, 0xA5, 0xB6, 
//     0xE7, 0x9A, 0x86, 
//     0xE6, 0x98, 0x8C, 
//     0x00, 0x00, 0x00, 
//     //回车换行\r\n
//     // 0x0D, 0x0A, 
//     0x22, 
//     0xff, 0xff, 0xff};

//     for(i=0;i<12;i++)
//     {
//         buf[i+18] = name[i];
//     }

//     Uart_Write_Data(buf, 34);
// }

//测试--成功
//串口屏打印：饶皆昌
void Display_Screen_Write_Name_tmp(void)
{
    // Display_Screen_Write((hi_u8 *)"t0.txt+="饶皆昌");
    hi_u8 buf[34] = {0x74, 0x30, 0x2E, 0x74, 0x78, 0x74, 0x2B, 0x3D, 0x22, 
    //姓名：E5A793 E5908D EFBC9A
    0xE5, 0xA7, 0x93, 
    0xE5, 0x90, 0x8D, 
    0xEF, 0xBC, 0x9A, 
    //饶皆昌
    0xE9, 0xA5, 0xB6, 
    0xE7, 0x9A, 0x86, 
    0xE6, 0x98, 0x8C, 
    0x00, 0x00, 0x00, 
    //回车换行\r\n
    // 0x0D, 0x0A, 
    0x22, 
    0xff, 0xff, 0xff};

    Uart_Write_Data(buf, 34);
}

//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////

//代码转换:从一种编码转为另一种编码
// int code_convert(unsigned char *from_charset,unsigned char *to_charset,unsigned char *inbuf,unsigned int inlen,unsigned char *outbuf,unsigned int outlen)
// {
//     iconv_t cd;
//     // int rc;
//     unsigned char **pin = &inbuf;
//     unsigned char **pout = &outbuf;

//     cd = iconv_open((const char *)to_charset,(const char *)from_charset);
//     if (cd==0) return -1;
//     memset(outbuf,0,outlen);
//     // if (iconv(cd,(char **)pin,&inlen,(char **)pout,&outlen)==-1) return -1;
//     iconv(cd,(char **)pin,&inlen,(char **)pout,&outlen);
//     iconv_close(cd);

//     return 0;
// }
// //UNICODE码转为GB2312码
// int u2g(unsigned char *inbuf,int inlen,unsigned char *outbuf,int outlen)
// {
//     return code_convert((unsigned char *)"utf-8",(unsigned char *)"gb2312",inbuf,inlen,outbuf,outlen);
// }
// //GB2312码转为UNICODE码
// int g2u(unsigned char *inbuf,size_t inlen,unsigned char *outbuf,size_t outlen)
// {
//     return code_convert((unsigned char *)"gb2312",(unsigned char *)"utf-8",inbuf,inlen,outbuf,outlen);
// }


// // void Display_Screen_Write(hi_u8 *data)
// // {
// //     hi_u8 tmp[3] = {0xFF, 0xFF, 0xFF};

// //     Uart_Write_Data(data, strlen((const char*)data));
// //     Uart_Write_Data(tmp, 3);
// // }
// // Display_Screen_Write((hi_u8 *)"t0.txt+=\"姓名：无\r\n\"");
// // Display_Screen_Write((hi_u8 *)"姓名：",P->data.name);
// #define OUT_LEN 255
// void Display_Screen_Write_utf8_to_gb2312(hi_u8 *data_in_utf8, hi_u8 *name)
// {
//     unsigned char out[OUT_LEN];
//     int rc;

//     //unicode码转为gb2312码
// 	//utf-8中文在数组中用16进制存储，可以转换为GB2312编码的数组。
// 	rc = u2g(data_in_utf8, strlen((const char *)data_in_utf8), out, OUT_LEN);
//     if(rc != 0)
//     {
//         printf("utf-8 to gb2312 fail.");
//     }
// 	// printf("unicode-->gb2312 out=%s\n",out);

//     //发送：[t0.txt+=\"]
//     Uart_Write_Data((hi_u8 *)"t0.txt+=\"", 9);

//     //发送：[姓名：]
//     Uart_Write_Data(data_in_utf8, strlen((const char*)data_in_utf8));

//     //发送：[*name]
//     Uart_Write_Data(name, strlen((const char*)name));

//     //发送：[\"]
//     Uart_Write_Data((hi_u8 *)"\"", 1);
// }
