## 基于 OpenHarmony 开源项目的刷卡考勤系统

#### 一、项目介绍
本作品系作者的毕业设计，基于 OpenHarmony 开源项目开发而成，实现以下功能：刷卡考勤、查看考勤记录、添加用户、删除用户、权限管理、系统设置等

**硬件**采用海思 Hi3861 2.4GHz Wi-Fi SoC 芯片为主控芯片，使用 RC522 模块进行刷卡录入考勤人员的信息，使用 USART 屏（串口屏）实现人机交互的UI设计；
**软件**设计包括使用 USART 屏实时显示刷卡考勤情况和查看考勤记录，操作 USART 屏添加用户和删除用户，
操作 USART 屏管理用户的权限和时长限制，通过基于 TCP 的应用层协议 MQTT 将考勤记录上传至华为云物联网平台。

由于作者的个人技术及时间等原因，作品没有做到很完善，仍有诸多的不足。如若有任何问题，可以通过邮箱联系作者，期待与您的沟通交流，相互学习(～^▽^)～ thx~

#### 二、版本

|版本|详情|时间|
|:-:|:-:|:-:|
|V0.0.0|移植RC522模块完成|2021.2.7|
|V0.0.1|添加链表和串口支持完成|2021.2.15|
|V1.0.0|刷卡考勤、查看考勤记录、添加用户、删除用户、权限管理、系统设置、上传考勤记录|2021.5底|

#### 三、开发环境介绍

软件：  
1. Visual Studio Code ( VSCode ): 代码编辑
2. Ubuntu: 代码编译
3. DevEco Device Tool: 代码烧录（VSCode 的插件）


#### 四、模块及外设介绍

使用模块情况：
1. 小熊派鸿蒙开发板：BearPi-HM_Nano ( 主控：海思 Hi3861 2.4GHz Wi-Fi SoC 芯片 )
2. RC522模块
3. USART屏

使用资源情况：
1. GPIO
2. UART
3. SPI
4. Flash
5. Wi-Fi
6. MQTT

<div align="center">

<img src="./picture/systemFrame.png" alt="系统实物图"/>

系统框图

<img src="./picture/systemProduct.jpg" width = "580" height = "400" alt="系统实物图"/>

系统实物图

</div>

#### 五、使用说明

1.  获取到 HarmonyOS 源码
>源码地址(点击下载)：https://repo.huaweicloud.com/harmonyos/os/1.0/code-1.0.tar.gz

2.  将文件夹 scancard_iot 拖到下载的源码目录 code-1.0/applications/sample/wifi-iot/app 下
3.  用仓库根目录下的 BUILD.gn 替换源码目录 code-1.0/applications/sample/wifi-iot/app 下的 BUILD.gn
4.  编译并测试。

```
python build.py wifiiot
```

#### 六、参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

#### 七、联系作者

	1148924656@qq.com






















